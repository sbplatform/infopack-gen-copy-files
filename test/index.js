var infopack = require('infopack');
var copyFiles = require('../');

infopack.evaluate(copyFiles.step({
    instructions: [{
        readFrom: 'markup_files/deep_folder',
        pattern: '**'
    }, {
        readFrom: 'raw_files',
        pattern: '*.yml'
    }]
})).then(function(data) {
    console.log(data);
});