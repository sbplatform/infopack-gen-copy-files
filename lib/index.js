var Promise = require('bluebird');
var fs = require('fs');
var path = require('path');
var copy = require('copy');

const DEFAULT_WRITE_PATH = 'output_files';

function step(settings) {
    return {
        settings: settings || {},
        run: run
    };
}

function run(pipeline, settings) {
    settings = settings || {}
    var _instructions = settings.instructions || [];

    return Promise.mapSeries(_instructions, function(instruction) {
        return new Promise(function(resolve, reject) {
            var readFrom = instruction.readFrom || settings.readFrom || '.';
            instruction.dest = path.resolve(readFrom, path.relative(readFrom, ''), instruction.dest || settings.dest || DEFAULT_WRITE_PATH);
            copy(instruction.pattern, instruction.dest, { dot: true, cwd: readFrom }, (err, files) => {
                if(err) {
                    throw new Error(err);
                }
                pipeline.addLog(`${files.length} were copied`);
                resolve(files);
            });
        });
    });
}

module.exports.step = step;
module.exports.run = run;
